---
- name: install requirements
  yum:
    name: "{{ item }}"
    state: present
  with_items:
    - grub2-efi
    - grub2-efi-modules
    - shim
    - syslinux

- name: create required folders
  file:
    path: "{{ item }}"
    state: directory
    owner: root
    group: root
    mode: 0755
  with_items:
    - "{{ tftp_root_directory }}/images/centos7"
    - "{{ tftp_root_directory }}/boot/EFI/centos/x86_64-efi"
    - "{{ tftp_root_directory }}/boot/pxelinux.cfg"

- name: copy centos kernel and initramfs to the tftp folder
  copy:
    src: "{{ rootfs_centos_dir }}/boot/{{ item }}"
    dest: "{{ tftp_root_directory }}/images/centos7"
    remote_src: true
    owner: root
    group: root
    mode: 0644
  with_items:
    - "vmlinuz-{{ rootfs_centos_kernel_version }}"
    - "initramfs-{{ rootfs_centos_kernel_version }}-nfs.img"
    - "vmlinuz-{{ rootfs_centos_kernel_rt_version }}"
    - "initramfs-{{ rootfs_centos_kernel_rt_version }}-nfs.img"

- name: copy efi files for UEFI network boot
  copy:
    src: "/boot/efi/EFI/centos/{{ item }}"
    dest: "{{ tftp_root_directory }}/boot"
    remote_src: true
    owner: root
    group: root
  with_items:
    - grubx64.efi
    - shim.efi

- name: copy *.mod and *.lst files for UEFI network boot
  shell: >
    cp /usr/lib/grub/x86_64-efi/*.{mod,lst} "{{ tftp_root_directory }}/boot/EFI/centos/x86_64-efi"
  args:
    creates: "{{ tftp_root_directory }}/boot/EFI/centos/x86_64-efi/command.lst"

- name: create grub configuration
  template:
    src: grub.cfg.j2
    dest: "{{ tftp_root_directory }}/boot/grub.cfg"
    owner: root
    group: root
    mode: 0644

- name: copy pxelinux.0 for PXE boot
  copy:
    src: "/usr/share/syslinux/{{ item }}"
    dest: "{{ tftp_root_directory }}/boot"
    remote_src: true
    owner: root
    group: root
    mode: 0644
  with_items:
    - pxelinux.0

- name: copy menu.c32 for PXE boot
  copy:
    src: "/usr/share/syslinux/{{ item }}"
    dest: "{{ tftp_root_directory }}/boot/pxelinux.cfg"
    remote_src: true
    owner: root
    group: root
    mode: 0644
  with_items:
    - menu.c32

- name: create pxelinux default configuration
  template:
    src: pxelinux.cfg.j2
    dest: "{{ tftp_root_directory }}/boot/pxelinux.cfg/default"
    owner: root
    group: root
    mode: 0644
