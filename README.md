ics-ans-role-rootfs-centos
==========================

Ansible role to install the EEE CentOS root file system.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
rootfs_centos_version: v0.5.1
rootfs_centos_archive: "centos7-ess-rootfs-{{ rootfs_centos_version }}.tar.bz2"
rootfs_centos_archive_url: "https://artifactory.esss.lu.se/artifactory/swi-pkg/EEE/centos/{{ rootfs_centos_archive }}"
rootfs_centos_kernel_version: "3.10.0-693.21.1.el7.x86_64"
rootfs_centos_kernel_rt_version: "3.10.0-693.21.1.rt56.639.el7.x86_64"
rootfs_centos_dir: /export/nfsroots/centos7/{{ rootfs_centos_version }}/rootfs
rootfs_centos_eee_server: "{{ ansible_default_ipv4.address }}"
rootfs_centos_nfs_mountpoints:
  - src: "{{ rootfs_centos_eee_server }}:/export/epics"
    mountpoint: /opt/epics
    rw: ro
  - src: "{{ rootfs_centos_eee_server }}:/export/startup"
    mountpoint: /opt/startup
    rw: ro
  - src: "{{ rootfs_centos_eee_server }}:/export/nonvolatile"
    mountpoint: /opt/nonvolatile
    rw: rw
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rootfs-centos
```

License
-------

BSD 2-clause
