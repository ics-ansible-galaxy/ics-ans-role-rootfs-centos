import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

rootfs_centos_version = 'v0.5.1'


def test_rootfs_fstab(host):
    fstab = host.file('/export/nfsroots/centos7/{}/rootfs/etc/fstab'.format(rootfs_centos_version))
    assert fstab.is_file
    assert '/var' not in fstab.content_string
    assert ':/export/epics  /opt/epics  nfs4    defaults,async,timeo=14,ro   0   0' in fstab.content_string
    assert ':/export/startup  /opt/startup  nfs4    defaults,async,timeo=14,ro   0   0' in fstab.content_string
    assert ':/export/nonvolatile  /opt/nonvolatile  nfs4    defaults,async,timeo=14,rw   0   0' in fstab.content_string


@pytest.mark.parametrize('filename', [
    'vmlinuz-3.10.0-693.21.1.el7.x86_64',
    'vmlinuz-3.10.0-693.21.1.rt56.639.el7.x86_64'
])
def test_centos_kernels(host, filename):
    assert host.file(os.path.join('/var/lib/tftpboot/images/centos7', filename)).exists


def test_menuc32_in_proper_directory(host):
    assert host.file('/var/lib/tftpboot/boot/pxelinux.cfg/menu.c32').exists


@pytest.mark.parametrize('mountpoint', [
    '/opt/epics',
    '/opt/startup',
    '/opt/nonvolatile',
])
def test_nfs_mountpoints_exist(host, mountpoint):
    assert host.file('/export/nfsroots/centos7/{}/rootfs{}'.format(
        rootfs_centos_version, mountpoint)).is_directory
